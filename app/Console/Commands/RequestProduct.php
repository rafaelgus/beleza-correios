<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\ProductRequestCheck;

use Correios;

class RequestProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'productrequest:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revisa y actualiza el status del envio del producto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productsreqs = ProductRequestCheck::all();

        $productsreqs->each(function($productreq, $key) {

            $trackNumber = $productreq->request_track;
            $rastreo = Correios::rastrear($trackNumber);
            $productId = $productreq->id;

            $sendStatus = ProductRequestCheck::where('id', $productId)
            ->update([
                'send_date'=> $rastreo[0]['data'],
                'send_status' => $rastreo[0]['status']
                ]);

        });



    }
}
