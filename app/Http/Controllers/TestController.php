<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Correios;

use \Bdourado\SigepCorreios\SigepCorreios;

class TestController extends Controller
{
    public function CoreiosCep()
    {
        $cepnumber = '89062086';

        $cep = Correios::cep($cepnumber);

        dd($cep);
    }

    public function coreiosRastreo()
    {
        $number = 'OD413758665BR';

        $cepRastreo = Correios::rastrear($number);

        //dd($cepRastreo[0]['status']);
        dd($cepRastreo);
    }

    public function coreiosRas()
    {
        $number = 'OD413758665BR';

        $codObjeto = 'OD413758665BR';
        $sro = SigepCorreios::getRastro($codObjeto);


        dd($sro);
    }

    public function coreiosCep2()
    {

        $cep = '7020003';
        $idServico = '124849';
        $qtdEtiquetas = '1';
        $sro = new SigepCorreios();
        $sro->solicitaEtiquetas($idServico, $qtdEtiquetas);


      dd($sro->__last_response);
    }
}
