<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sample1', function () {
    return view('samples.sample1');
});


Route::get('/coreios', 'TestController@CoreiosCep')->name('coreios-cep');

Route::get('/rastrear', 'TestController@coreiosRastreo')->name('coreios-rastreo');

Route::get('/rastrear-2', 'TestController@coreiosRas')->name('coreios-rastreo-2');

Route::get('/cep-2', 'TestController@coreiosCep2')->name('coreios-cep-2');


Route::get('/testing', function () {
    $config = array(
        "trace" => 1,
        "exception" => 0,
        "cache_wsdl" => WSDL_CACHE_MEMORY
    );

    $address = "https://apphom.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl";

    $client = new SoapClient($address, $config);

    //dd($client->__getTypes()[95]);

    $cep = '07070120';
    $etiquetas = 'DL61145929 BR';
    $codAdministrativo = '17000190';
    $numeroServico = '04162';
    $cepOrigem = '05311900';
    $cepDestino = '07070120';
    $usuario = 'sigep';
    $senha = 'n5f9t8';
    $tipoDestinatario = 'C';
    $identificador = '00000000000000';
    $idServico = '124849';
    $qtdEtiquetas = 1;

    $dados = array(

        'tipoDestinatario' => $tipoDestinatario,
        'identificador' => $identificador,
        'idServico' => $idServico,
        'qtdEtiquetas' => $qtdEtiquetas,
        'usuario' => $usuario,
        'senha' => $senha

    );

    $sigep = $client->solicitaEtiquetas($dados);

    dd($sigep);
});
